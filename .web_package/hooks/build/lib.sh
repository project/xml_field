#!/usr/bin/env bash
cd $7
mkdir $7/temp
cd $7/temp && grab loft_php_lib -f
mv $7/temp/loft_php_lib/src/AKlump/LoftLib/Xml/LoftXmlElement.php $7/includes/
rm -r $7/temp
